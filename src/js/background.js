chrome.runtime.onInstalled.addListener(function () {
    $.ajax({
        url: 'http://localhost:3704/api/vcost/Settings/ExtraInfo',
        type: "GET",
        cache: false,
        dataType: "json",
        data: {}
    }).success(function (responseData) {
        chrome.storage.local.set({ 'parserSettings': responseData }, function (eventState) {
            console.log("settings loaded");
        });
    }).error(function (err) {
        console.log(err);
    });
});

chrome.runtime.onMessage.addListener(function (msg, sender) {
    // First, validate the message's structure
    if ((msg.from === 'content') && (msg.subject === 'showPageAction')) {
        // Enable the page-action for the requesting tab
        chrome.pageAction.show(sender.tab.id);
    }
});