function Parser(data) {
    this.apiHostUrl = "http://localhost:3704/api/vcost";
    this.settings = data;
}

Parser.prototype.start = function (url, onSuccess) {
    console.log('Parser start');

    var self = this;
    if (self.settings == null || self.settings.Sources == null) {
        console.log("settings not loaded");
        return false;
    };

    $.each(self.settings.Sources, function () {
        var source = this;

        console.log('Parser URL:' + url);

        if ((new RegExp(source.UrlPattern)).test(url)) {
            var adInfo = {
                SourceId: source.SourceId,
                Url: url,
                Properties: self.parsePage(source)
            };

            console.log('Parser sending info');

            self.sendAdInfo(adInfo, onSuccess);

            return;
        }
    });
};

Parser.prototype.sendAdInfo = function (adInfo, onSuccess) {
    var self = this;

    $.ajax({
        url: self.apiHostUrl + '/ads',
        type: "POST",
        cache: false,
        contentType: 'application/json',
        data: JSON.stringify(adInfo)
    }).done(function (data, textStatus, request) {
        var idString = request.getResponseHeader('location');

        if (idString == null) {
            return;
        }

        var id = idString.trim().split("/")[2];
        if (parseInt(id) != NaN) {
            self.getExtraInfo(id, onSuccess);
        }
    }).fail(function (data, textStatus, request) {
        console.log(data);
    });
};

Parser.prototype.getExtraInfo = function (id, onSuccess) {
    var self = this;

    console.log('getExtraInfo called');

    $.ajax({
        url: self.apiHostUrl + '/ads/' + id + '/ExtraInfo',
        type: "GET",
        cache: false,
        contentType: 'application/json',
        data: {}
    }).success(function (data, textStatus, request) {
        if (data == null) {
            return;
        }

        console.log('getExtraInfo success');

        if (typeof onSuccess == 'function') {
            onSuccess(data);
        }
    }).error(function (data, textStatus, request) {
        console.log(data);
    });
};

Parser.prototype.parsePage = function (source) {
    var properties = [];

    $.each(source.Properties, function () {
        var property = this;

        var value = null;
        if (property.Attr != undefined) {
            value = $(property.Css).attr(property.Attr);
        } else {
            value = $(property.Css).text();
        }

        properties.push({
            Name: property.Name,
            Value: value
        });
    });

    return properties;
};