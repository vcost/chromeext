chrome.storage.local.get('parserSettings', function (data) {
    if (!data) {
        console.log("settings not loaded");
        return;
    }

    var url = window.location.href;
    
    $.each(data.parserSettings.Sources, function () {
        var source = this;
        if ((new RegExp(source.UrlPattern)).test(url)) {
            chrome.runtime.sendMessage({
                from: 'content',
                subject: 'showPageAction'
            });
        }
    });
});