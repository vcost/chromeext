jQuery(function ($) {
    chrome.storage.local.get('parserSettings', function (data) {
        if (!data) {
            console.log("settings not loaded");
            return;
        }

        console.log("settings loaded");

        chrome.tabs.getSelected(null, function (tab) {
            var tablink = tab.url;

            window.parser = new Parser(data.parserSettings);

            $("#loader").show();
            $("#result").hide();

            window.parser.start(tablink, function (info) {
                var template = null;
                if (info == null) {
                    var source = $("#empty-data-template").html();
                    template = Handlebars.compile(source);
                } else {
                    var source = $("#data-template").html();
                    template = Handlebars.compile(source);
                }

                $("#loader").hide();
                $("#result").show();

                $("#result").html(template(info));
            });
        });

    });
});